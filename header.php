<?php   require_once ('function_php/function.php'); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
		<link rel="stylesheet" href="css/forum.css">
		<link rel="stylesheet" href="	css/colorpicker.css">
		<link rel="stylesheet" href="css/layout.css">
		<title>forum saint-seiya RP</title>
	</head>

<?php if ((isset($_GET['idf']))&& (getFather($_GET['idf']))):
	$value=getForumId($_GET['idf']);

?>
	<body class="<?php echo $value->name; ?>" >
<?php elseif ((isset($_GET['idf']))&& (!(getFather($_GET['idf'])))):
	$value=getForumFather($_GET['idf']);
	while ($value!=null) {
		$tmp=$value->idforum;
		$tmp2=$value->name;
		$value=getForumFather($tmp);
	}
?>
	<body class="<?php echo $tmp2;?>">
<?php endif; ?>
<?php
		session_start ();
	?>
		<div id="banner" class="banniere"></div>
<h1>
Saint Seiya RP

</h1>
<div class="navbar">
	<ul class="linklist navlinks">
		<li>
			<a class="mainmenu" href="forum.php">	Forum</a>
		</li>
		<li>
			<a class="mainmenu" href="membre.php"> Membres</a>
		</li>
		<li>
			<a class="mainmenu" href="formulaire.php"> Inscription</a>
		</li>
		<?php	if(!isset($_SESSION['name']) && !isset($_SESSION['ranked'])&& !isset($_SESSION['idtable1'])) : 	?>
			<li>
				<a class="mainmenu" href="authentification.php"> Connexion</a>
			</li>
		<?php else: ?>
			<li>
				<a class="mainmenu" href="compte.php"> Profil</a>
			</li>
		<?php endif; ?>

		<?php	if(isset($_SESSION['name']) && isset($_SESSION['ranked'])&& isset($_SESSION['idtable1'])) : ?>
			<li>
				<a class="mainmenu" href="logout.php"> Déconnexion</a>
			</li>
		<?php endif; ?>

	</ul>
</div>
