<?php
require_once ('connec_db.php');

function getAddNewMessage($params,$idForum,$idUser){
  global $pdo;
    $query = "INSERT INTO message VALUES(default, NOW(), default, :textMessage,default, :fk_compte);";
    $prep= $pdo->prepare($query);
    $prep->bindvalue(':textMessage',$params['submitMessage'], PDO::PARAM_STR);
    $prep->bindvalue(':fk_compte',$idUser, PDO::PARAM_INT);
    $prep->execute();
    $idMessage=$pdo->lastInsertId();

    $query2="INSERT INTO forum_has_message VALUES (:forum_idforum,  :message_idmessage)";
    $prep= $pdo->prepare($query2);
    $prep->bindvalue(':forum_idforum',$idForum, PDO::PARAM_INT);
    $prep->bindvalue(':message_idmessage',$idMessage, PDO::PARAM_INT);
    $prep->execute();
}

function getAddNewMessageDm($params,$idCompte,$idSession){
  global $pdo;
    $query = "INSERT INTO message VALUES(default, NOW(), default, :textMessage,default, :fk_compte);";
    $prep= $pdo->prepare($query);
    $prep->bindvalue(':textMessage',$params['submitDm'], PDO::PARAM_STR);
    $prep->bindvalue(':fk_compte',$idSession, PDO::PARAM_INT);
    $prep->execute();
    $idMessage=$pdo->lastInsertId();

    $query2="INSERT INTO compte_has_message VALUES (:id_compte,  :id_message)";
    $prep= $pdo->prepare($query2);
    $prep->bindvalue(':id_compte',$idCompte, PDO::PARAM_INT);
    $prep->bindvalue(':id_message',$idMessage, PDO::PARAM_INT);
    $prep->execute();
}

function getAuthentification($pdo,$login,$pass){
  $query = "SELECT * FROM Compte where name=:login and password=:pass";
  $prep = $pdo->prepare($query);
  $prep->bindValue(':login', $login);
  $prep->bindValue(':pass', $pass);
  $prep->execute()
;
  // on vérifie que la requête ne retourne qu'une seule ligne
  if($prep->rowCount() == 1){
    $result = $prep->fetch(PDO::FETCH_ASSOC);
    return $result;
  }
 else
  return false;
}

function getDM($id)
{
  global $pdo;
     $query = "SELECT message.textMessage,C2.name FROM message, compte_has_message, compte C1, compte C2 WHERE
                                                                 C1.idtable1=compte_has_message.id_compte
                                                                 AND compte_has_message.id_message=message.idmessage
                                                                 AND message.fk_compte=C2.idtable1
                                                                 AND C1.idtable1=:id;";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}

function getMessageForum($id){
  global $pdo;
     $query = "SELECT message.textMessage FROM message, forum_has_message, forum WHERE message.idmessage=forum_has_message.message_idmessage
                                                                 AND forum_has_message.forum_idforum=forum.idforum
                                                                 AND forum.idforum=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}

function getNameForum($id){
  global $pdo;
     $query = "SELECT * FROM forum WHERE forum.forum_father_idforum_father=:id";

     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}

function getMembre(){
  global $pdo;
     $query = "SELECT * FROM compte";
     $prep = $pdo->prepare($query);
     $prep->execute();
     return $prep->fetchAll();
}

function getMembreId($id){
  global $pdo;
     $query = "SELECT * FROM compte WHERE idtable1=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id,PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetch();
}

function getForumId($id){
  global $pdo;
     $query = "SELECT * FROM forum WHERE idforum=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetch();

}

function getFather($id){
  global $pdo;
     $query = "SELECT * FROM forum WHERE idforum=:id AND forum_father_idforum_father is null";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetch();
}

function getForumFatherNull($id){
  global $pdo;
     $query = "SELECT * FROM forum WHERE forum_father_idforum_father is null
                                         AND idforum=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetch();

}

function getForumFather($id){
  global $pdo;
     $query = "SELECT F2.name, F2.idforum FROM forum F1, forum F2 WHERE F1.idforum=:id
                                                                  AND F1.forum_father_idforum_father is not null
                                                                  AND F1.forum_father_idforum_father=F2.idforum;";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetch();

}



function getSonforum($id){
  global $pdo;
     $query = "SELECT * FROM forum F1 WHERE NOT EXISTS
                                                (SELECT * FROM forum F2 WHERE F2.forum_father_idforum_father=F1.idforum)
                                                AND EXISTS (select * FROM forum F3 WHERE F1.forum_father_idforum_father=F3.idforum AND F3.idforum=:id)";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();

}

function getUserMessageName($id){
  global $pdo;
     $query = "SELECT compte.name, compte.idtable1 FROM compte,message,forum_has_message,forum WHERE compte.idtable1=message.fk_compte AND message.idmessage=forum_has_message.message_idmessage
                                                                           AND forum_has_message.forum_idforum=forum.idforum
                                                                           AND forum.idforum=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}


function getAjout($params){
  global $pdo;
      $query = "INSERT INTO compte VALUES(default, default, :password, :clan, :name, :email);";
    $prep= $pdo->prepare($query);
    $prep->bindvalue(':password',$params['password'], PDO::PARAM_STR);
    $prep->bindValue(':clan',$params['clan'], PDO::PARAM_STR);
    $prep->bindValue(':name',$params['name'], PDO::PARAM_STR);
    $prep->bindValue(':email',$params['email'],PDO::PARAM_STR);
    $prep->execute();
}


function getModification($params){
  global $pdo;
    $query = "UPDATE compte SET ranked =:ranked, password =:password, clan =:clan, name =:name, email=:email WHERE idtable1 = :id;";
    $prep= $pdo->prepare($query);

    $prep->bindValue(':ranked',$params['ranked'], PDO::PARAM_STR);
    $prep->bindvalue(':password',$params['password'], PDO::PARAM_INT);
    $prep->bindvalue(':clan',$params['clan'], PDO::PARAM_STR);
    $prep->bindValue(':name',$params['name'], PDO::PARAM_STR);
    $prep->bindValue(':email',$params['email'], PDO::PARAM_STR);
    $prep->bindvalue(':id',$params['id'], PDO::PARAM_INT);
    $prep->execute();

}

function getDelete($id){
  global $pdo;
    $query="DELETE FROM compte WHERE idtable1= :id;";
    $prep = $pdo->prepare($query);
    $prep->bindValue(':id',$id, PDO::PARAM_STR);
    $prep->execute();
}


?>
